
// Задание 1 - Имитируем работу с сервером - Promise
// function getList(){
//     return  new Promise((resolve,reject)=>{
//         const list= ([{ id: 1, title: 'Task 1', isDone: false },
//         { id: 2, title: 'Task 2', isDone: true }]);
//         setTimeout(()=>{
//             resolve(list);
//         },2000)
    
//         setTimeout(()=>{
//             reject('error')
//         },2000)
//     })

// }

// getList().then(list=>console.log(list))
// .catch(err=>console.log(err));

// Задание 2 - Чейнинг (цепочки) промисов
// *
// * Написать функцию которая будет соберет строку "Я использую цепочки обещаний", конкотенируя каждое
// * слово через отдельный then блок с задержкой в 1 секунду на каждой итерации.
// * Результат вывести в консоль.

// const getString=()=>new Promise(resolve=>setTimeout(()=>resolve('Я'),1000))
// .then(result=>new Promise((resolve)=>setTimeout(()=>resolve(result+' использую'),1000)))
// .then(result=>new Promise((resolve)=>setTimeout(()=>resolve(result+' цепочки'),1000)))
// .then(result=>new Promise((resolve)=>setTimeout(()=>resolve(result+' обещаний'),1000)));

// getString().then(result=>console.log(result));




// * Задание 3 - Параллельные обещания
// *
// * Написать функцию которая будет соберет строку "Я использую вызов обещаний параллельно",
// * используя функцию Promise.all(). Укажите следующее время задержки для каждого
// * промиса возвращаего слова:
// * Я - 1000,
// * использую - 800
// * вызов - 1200
// * обещаний - 700
// * параллельно - 500
// * Результат вывести в консоль.
// */



// const getString=()=>{
//     return Promise.all([
//         new Promise(resolve=>setTimeout(()=>resolve('Я'),1000)),
//         new Promise(resolve=>setTimeout(()=>resolve('использую'),800)),
//         new Promise(resolve=>setTimeout(()=>resolve('вызов'),1200)),
//         new Promise(resolve=>setTimeout(()=>resolve('обещаний'),700)),
//         new Promise(resolve=>setTimeout(()=>resolve('параллельно'),500))
        
//     ]).then(result=>result.join(' '))
// }
// getString().then(result=>console.log(result));




// * Задание 4 - Напишите функцию delay(ms), которая возвращает промис,
// * переходящий в состояние "resolved" через ms миллисекунд.
// *
// * delay(2000).then(() => console.log('Это сообщение вывелось через 2 секунды'))
// */

// delay = ms => {
//     return new Promise(res => {
//         setTimeout(() => res('Это сообщение вывелось через 2 секунды'), ms)
//     })
//  }
//  delay(2000).then(res => console.log(res));




/**
 * Задание 5 - Решите 3 задачу, используя, функцию delay
 */
// delay = (a,b,c,d,e) => {
//     return Promise.all([
//             new Promise(res => {
//                 setTimeout(() => res("Я"), a)
//             }),
//             new Promise(res => {
//                 setTimeout(() => res("использую"), b)
//             }),
//             new Promise(res => {
//                 setTimeout(() => res("вызов"), c)
//             }),
//             new Promise(res => {
//                 setTimeout(() => res("обещаний"), d)
//             }),
//             new Promise(res => {
//                 setTimeout(() => res("параллельно"), e)
//             })
//         ]
//     ).then(result => result.join(' '))
// }
// delay(1000,800,1200,700,500).then(result => console.log(result));






// * Задание 6 - Напишите функцию, которая загрузит данные по первому фильму в котором встретилась планета Татуин, используя
// * предоставленный API (https://swapi.dev)




// async function getFilmName(planetName) {
//     const data = await fetch('https://swapi.dev/api/films/').then(res => res.json());
//     const films = data.results;
//     for (let i = 0; i < films.length; i++) {
//         const planets = await Promise.all(films[i].planets.map(planetUrl => fetch(planetUrl).then(res => res.json())));
//         if (planets.find(planet => planet.name === planetName)) {
//             return films[i].title
//         }
//     }
//     return 'Фильм не найден'
// }
// getFilmName('Tatooine').then(res => console.log(res));




/**
 * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
 * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
 * полезной нагрузкой:
 * { message: "Привет сервис, я жду от тебя ответа"}
 * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
 */



/**
 * Задание 7 - Напишите функцию, которая выведет название транспортного средства на котором впервые ехал Anakin Skywalker, используя
 * предоставленный API (https://swapi.dev)
 */

// (async () => {
//  const person = await fetch ('https://swapi.dev/api/people/1/').then(response => response.json());
//     const starshipUrl = person.starships[0]
//     const starshipName = (await fetch(starshipUrl).then(response => response.json())).name
//     console.log(starshipName)
//     return starshipName
// })();
// * Задание 8 - Напишите эхо-сервер, который на запрос POST /echo будет возвращать тело запроса.
// * Напишите функцию которая посылает запрос на http://localhost:3000 POST /echo со следующей
// * полезной нагрузкой:
// * { message: "Привет сервис, я жду от тебя ответа"}
// * Примите ответ от сервера и выведите результат в консоль, используя синтаксис async/await.
// */
//  const fetch = require("node-fetch");
//  const http = require("http");
//  const url = require("url");
 
//  const server = new http.Server(function (req, res) {
//    const urlParsed = url.parse(req.url, true);
 
//    if (urlParsed.pathname == "/echo" && req.method == "POST") {
//      req.on("data", (d) => {
//        const message = JSON.parse(d).message;
//        console.log(message);
//      });
//    } else {
//      res.statusCode = 404;
//      console.log("Page not found");
//      res.end("Page not found");
//    }
//  });
 
//  server.listen(3000, () => {
//    console.log("start");
//  });
 
//  (async () => {
//    await fetch("http://localhost:3000/echo", {
//      method: "POST",
//      body: JSON.stringify({ message: "Привет сервис, я жду от тебя ответа" }),
//    });
//  })();
 